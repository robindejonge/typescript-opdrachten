class Game {
    constructor(canvasId) {
        this.player = "Player1";
        this.score = 400;
        this.lives = 3;
        this.canvas = canvasId;
        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;
        this.ctx = this.canvas.getContext('2d');
        this.highscores = [
            {
                playerName: 'Loek',
                score: 40000
            },
            {
                playerName: 'Daan',
                score: 34000
            },
            {
                playerName: 'Rimmert',
                score: 200
            }
        ];
        this.title_screen();
    }
    start_screen() {
        this.writeTitle();
        this.writePlay();
        this.drawButton();
        this.drawAsteroid();
    }
    writeTitle() {
        this.ctx.fillStyle = '#fff';
        this.ctx.font = '140px minecraft';
        this.ctx.textAlign = 'center';
        this.ctx.fillText('Asteroids', this.canvas.width / 2, this.canvas.height / 2 - 100);
    }
    writePlay() {
        this.ctx.fillStyle = '#fff';
        this.ctx.font = '50px minecraft';
        this.ctx.textAlign = 'center';
        this.ctx.fillText('Press play to start!', this.canvas.width / 2, this.canvas.height / 2 + 150);
    }
    drawButton() {
        let img = new Image();
        img.src = 'assets/images/SpaceShooterRedux/PNG/UI/buttonBlue.png';
        img.onload = () => {
            this.ctx.drawImage(img, this.canvas.width / 2 - 110, this.canvas.height / 1.26, 222, 80);
            this.ctx.fillStyle = 'black';
            this.ctx.font = '60px minecraft';
            this.ctx.fillText('PLAY', this.canvas.width / 2, this.canvas.height / 1.165);
        };
    }
    drawAsteroid() {
        let img = new Image();
        img.src = 'assets/images/SpaceShooterRedux/PNG/Meteors/meteorBrown_big3.png';
        img.onload = () => {
            this.ctx.drawImage(img, this.canvas.width / 2 - 30, this.canvas.height / 2);
        };
    }
    level_screen() {
        this.drawPlayerLives();
        this.writeCurrentScore(this.canvas.width - 275, 60);
        this.drawRandomAsteroids();
        this.drawSpaceShip();
    }
    drawPlayerLives() {
        let totalLives = 3;
        for (let i = 0; i < totalLives; i++) {
            let img = new Image();
            img.src = 'assets/images/SpaceShooterRedux/PNG/UI/PlayerLife1_green.png';
            img.onload = () => {
                this.ctx.drawImage(img, 40 + (i * 40), 40);
            };
        }
    }
    writeCurrentScore(xPos, yPos) {
        let currentScore = 2500;
        this.ctx.fillStyle = '#fff';
        this.ctx.font = '25px minecraft';
        this.ctx.textAlign = 'center';
        this.ctx.fillText(`Score: ${currentScore}`, xPos, yPos);
    }
    drawRandomAsteroids() {
        let meteors = ['meteorBrown_big1', 'meteorBrown_big2', 'meteorBrown_big3', 'meteorBrown_med1', 'meteorBrown_med3', 'meteorBrown_small1'];
        for (let i = 0; i < meteors.length; i++) {
            let img = new Image();
            img.src = `assets/images/SpaceShooterRedux/PNG/Meteors/${meteors[i]}.png`;
            img.onload = () => {
                let randomWidth = this.randomNumber(0, this.canvas.width);
                let randomHeight = this.randomNumber(0, this.canvas.height);
                this.ctx.drawImage(img, randomWidth, randomHeight);
            };
        }
    }
    drawSpaceShip() {
        let img = new Image();
        img.src = 'assets/images/SpaceShooterRedux/PNG/playerShip2_blue.png';
        img.onload = () => {
            this.ctx.drawImage(img, this.canvas.width / 2 - 56, this.canvas.height / 1.4);
        };
    }
    title_screen() {
        this.writeCurrentScore(this.canvas.width / 2, this.canvas.height / 4);
        this.writeHighScores();
    }
    writeHighScores() {
        this.ctx.fillStyle = '#fff';
        this.ctx.font = '60px minecraft';
        this.ctx.textAlign = 'center';
        for (let i = 0; i < 3; i++) {
            this.ctx.fillText(this.highscores[i].score, this.canvas.width / 2, 500 + i * 80);
        }
    }
    randomNumber(min, max) {
        return Math.round(Math.random() * (max - min) + min);
    }
}
let init = function () {
    const Asteroids = new Game(document.getElementById('canvas'));
};
window.addEventListener('load', init);
//# sourceMappingURL=app.js.map