class Game {
    //global attr for canvas
    //readonly attributes must be initialized in the constructor
    private readonly canvas: HTMLCanvasElement; // find the right type
    private readonly ctx: CanvasRenderingContext2D; // find the right type

    //some global player attributes
    private readonly player: string = "Player1";
    private readonly score: number = 400;
    private readonly lives: number = 3;
    private readonly highscores: Array<any>; //TODO: do not use 'any': write an interface!

    public constructor(canvasId: HTMLCanvasElement) {
        //construct all canvas
        this.canvas = canvasId;
        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;
        //set the context of the canvas
        this.ctx = this.canvas.getContext('2d');

        this.highscores = [
            {
                playerName: 'Loek',
                score: 40000
            },
            {
                playerName: 'Daan',
                score: 34000
            },
            {
                playerName: 'Rimmert',
                score: 200
            }
        ]

        // all screens: uncomment to activate 
        //this.start_screen();
        //this.level_screen();
        this.title_screen();

    }

    //-------- Splash screen methods ------------------------------------
    /**
     * Function to initialize the splash screen
     */
    public start_screen() {
        //1. add 'Asteroids' text
        this.writeTitle()
        //2. add 'Press to play' text
        this.writePlay();
        //3. add button with 'start' text
        this.drawButton();
        //4. add Asteroid image
        this.drawAsteroid();
    }

    public writeTitle()
    {
        this.ctx.fillStyle = '#fff';
        this.ctx.font = '140px minecraft';
        this.ctx.textAlign = 'center';
        this.ctx.fillText('Asteroids', this.canvas.width/2, this.canvas.height/2-100);
    }

    public writePlay()
    {
        this.ctx.fillStyle = '#fff';
        this.ctx.font = '50px minecraft';
        this.ctx.textAlign = 'center';
        this.ctx.fillText('Press play to start!', this.canvas.width/2, this.canvas.height/2+150);
    }

    public drawButton()
    {
        let img = new Image();
        img.src = 'assets/images/SpaceShooterRedux/PNG/UI/buttonBlue.png';
        img.onload = () =>
        {
            this.ctx.drawImage(img, this.canvas.width/2-110, this.canvas.height/1.26, 222, 80);
            this.ctx.fillStyle = 'black';
            this.ctx.font = '60px minecraft';
            this.ctx.fillText('PLAY', this.canvas.width/2, this.canvas.height/1.165);
        }      
    }

    public drawAsteroid()
    {
        let img = new Image();
        img.src = 'assets/images/SpaceShooterRedux/PNG/Meteors/meteorBrown_big3.png';
        img.onload = () =>
        {
            this.ctx.drawImage(img, this.canvas.width/2-30, this.canvas.height/2);
        }
    }

    //-------- level screen methods -------------------------------------
    /**
     * Function to initialize the level screen
     */
    public level_screen() {
        //1. load life images
        this.drawPlayerLives();
        //2. draw current score
        this.writeCurrentScore(this.canvas.width - 275, 60);
        //3. draw random asteroids
        this.drawRandomAsteroids();
        //4. draw player spaceship
        this.drawSpaceShip();
    }

    public drawPlayerLives()
    {
        let totalLives: number = 3;
        for(let i = 0; i < totalLives; i++)
        {
            let img = new Image();
            img.src = 'assets/images/SpaceShooterRedux/PNG/UI/PlayerLife1_green.png';
            img.onload = () =>
            {
                this.ctx.drawImage(img, 40 + (i * 40), 40);
            }
        }
    }

    public writeCurrentScore(xPos: number, yPos: number)
    {
        let currentScore: number = 2500;
        this.ctx.fillStyle = '#fff';
        this.ctx.font = '25px minecraft';
        this.ctx.textAlign = 'center';
        this.ctx.fillText(`Score: ${currentScore}`, xPos, yPos);
    }

    public drawRandomAsteroids()
    {
        let meteors = ['meteorBrown_big1', 'meteorBrown_big2', 'meteorBrown_big3', 'meteorBrown_med1', 'meteorBrown_med3', 'meteorBrown_small1'];
        
        for(let i = 0; i < meteors.length; i++)
        {
            let img = new Image();
            img.src = `assets/images/SpaceShooterRedux/PNG/Meteors/${meteors[i]}.png`
            img.onload = () =>
            {
                let randomWidth = this.randomNumber(0, this.canvas.width);
                let randomHeight = this.randomNumber(0, this.canvas.height);
                this.ctx.drawImage(img, randomWidth, randomHeight);
            }
        }
    }

    public drawSpaceShip()
    {
        let img = new Image();
        img.src = 'assets/images/SpaceShooterRedux/PNG/playerShip2_blue.png';
        img.onload = () =>
        {
            this.ctx.drawImage(img, this.canvas.width/2 - 56, this.canvas.height/1.4)
        }
    }

    //-------- Title screen methods -------------------------------------

    /**
    * Function to initialize the title screen   
    */
    public title_screen() {
        //1. draw your score
        this.writeCurrentScore(this.canvas.width/2, this.canvas.height/4);
        //2. draw all highscores
        this.writeHighScores();
    }

    public writeHighScores()
    {
        this.ctx.fillStyle = '#fff';
        this.ctx.font = '60px minecraft';
        this.ctx.textAlign = 'center';
        for(let i = 0; i < 3; i++)
        {
            this.ctx.fillText(this.highscores[i].score, this.canvas.width/2, 500 + i * 80);
        }
    }

    //-------Generic canvas functions ----------------------------------

    /**
    * Renders a random number between min and max
    * @param {number} min - minimal time
    * @param {number} max - maximal time
    */
    public randomNumber(min: number, max: number): number {
        return Math.round(Math.random() * (max - min) + min);
    }
}

//this will get an HTML element. I cast this element in de appropriate type using <>
let init = function () {
    const Asteroids = new Game(<HTMLCanvasElement>document.getElementById('canvas'));
};
//add loadlistener for custom font types
window.addEventListener('load', init);
